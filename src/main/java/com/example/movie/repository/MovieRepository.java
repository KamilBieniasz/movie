package com.example.movie.repository;

import com.example.movie.dto.MovieDto;
import java.util.List;


public interface MovieRepository {
    List<MovieDto> getMovie();
}
