package com.example.movie.repository;

import com.example.movie.dto.MovieDto;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;


@Repository
public class MovieRepositoryImpl implements MovieRepository{
    ArrayList<MovieDto> movieList = new ArrayList<>();
    public MovieRepositoryImpl(){
        movieList.add(new MovieDto.Builder().movieId(1).title("Piraci z krzemowej doliny").year(1999).image("https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg").build());
        movieList.add(new MovieDto.Builder().movieId(2).title("Ja, robot").year(1999).image("https://fwcdn.pl/fpo/54/92/95492/7521206.6.jpg").build());
        movieList.add(new MovieDto.Builder().movieId(3).title("Kod nieśmiertelności").year(1999).image("https://fwcdn.pl/fpo/89/67/418967/7370853.6.jpg").build());
        movieList.add(new MovieDto.Builder().movieId(4).title("Ex Machina").year(1999).image("https://fwcdn.pl/fpo/64/19/686419/7688121.6.jpg").build());
    }

    @Override
    public List<MovieDto> getMovie() {
        return movieList;
    }



}
