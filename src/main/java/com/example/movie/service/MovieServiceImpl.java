package com.example.movie.service;

import com.example.movie.dto.MovieDto;
import com.example.movie.dto.Movies;
import com.example.movie.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieServiceImpl implements MovieService {
    private final MovieRepository movieRepository;


    @Autowired
    public MovieServiceImpl(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @Override
    public List<MovieDto> getMovie() {
        return movieRepository.getMovie();
    }

    @Override
    public Movies returnMovies() {
        return new Movies(getMovie());
    }


}
