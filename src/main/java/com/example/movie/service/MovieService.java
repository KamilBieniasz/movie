package com.example.movie.service;

import com.example.movie.dto.MovieDto;
import com.example.movie.dto.Movies;

import java.util.List;
import java.util.Map;

public interface MovieService {
    List<MovieDto> getMovie();
    Movies returnMovies();
}
