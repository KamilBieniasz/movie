package com.example.movie.controller;

import com.example.movie.dto.MovieDto;
import com.example.movie.dto.Movies;
import com.example.movie.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class MoviesApiController {

    private final MovieService movieService;

    @Autowired
    public MoviesApiController(MovieService movieService){
        this.movieService = movieService;
    };

    @GetMapping("/movies")
    public ResponseEntity<Movies> getMovies(){
        return new ResponseEntity<>(movieService.returnMovies(), HttpStatus.OK);
    }



}