package com.example.movie.dto;

import java.util.List;

public class Movies {
    List<MovieDto> movies;

    public Movies(List<MovieDto> movies) {
        this.movies = movies;
    }

    public List<MovieDto> getMovies() {
        return movies;
    }
}


